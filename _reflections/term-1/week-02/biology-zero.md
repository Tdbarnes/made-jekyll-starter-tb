---
title: 02. Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
icon: images/bio-02.svg
published: true
---

![]({{site.baseurl}}/photo B0 grid.jpg)

This week we studied science, focussing on Biology. In recent years there has been an increase in people studying science at home and in workshops, and the increasing availability of tools and information means more people can work this way. In the same way a Fablab opens the field of design and digital fabrication, what ideas can be developed by more people working with Biology? To help us be able to start thinking about these topics we had a crash course in Biology. A course called Biology Zero because it is aimed at people who have no previous grounding in Biology. An overview of some of the basic principles and some practical activities which can be undertaken in a relatively low cost way in workshop or wet lab.

![]({{site.baseurl}}/photo_B0_grid3.jpg)

**Question:**

Can natural foragers help us obtain discarded materials more easily to make items in a FabLab??

Background - We took part in an activity in MDEF bootcamp week, hunting for thrown away materials that could be reused or upcycled using the FabLab equipment. We were looking in the area of Poblenou around IAAC. My interest is could this process take inspiration from natural foragers that exist in nature already? How are items found, what happens when they are, are these places remembered for future reference? There are lots of aspects to this topic. Initially however I am only concerned with the searching and location of materials.

Background research is required into more how various natural foragers optimise their chances of finding their food or building resources. Eusocial insects e.g ants. They use pheromone trails as a way of communicating where they have been which other ants follow to the food source.

Hypothesis: Natural foragers will increase access to learned pathways and materials within a neighbourhood of a city.

Method - Using a tracking app to assess speed in which materials are found. Will the tracking app increase the speed items are found?

**Experiment 1:**

There are two teams of foragers with two people in each team.
One team with a tracking app. One without.

The first member of each team goes out to find an item placed on a street. The item will be easily recognisable, but not from a distance or the start location.
Each team does it at separate times so they can’t see each other.
The time is recorded for how long it takes for the item to be found.

The next team member goes out and finds the same item, in the same location. One team has access to the route previously taken on the tracking app and has to follow it. The other does not, and has to find it on there own again.

The combined times for both team members is used to determine the times for team as a whole.

The process is repeated with new locations.

Possible outcomes:

-  1.1 The teams both take the same amount of time
-  1.2 The team without the tracking app takes less time than the one with one.
-  1.3 The team with the tracking app takes less time than the one without one.


Possible conclusions based on outcomes

1.1 If the times were the same then the tracking app didn’t affect the time it took to find items and the ability for team members to follow there partners makes no difference to the outcome.

1.2 The team without the tracking app could have taken less time because they found the quickest routes overall. Also if the first team member with the tracking app found the item using a really long route then the other one would be forced to take the same longer route.

1.3 The team with the tracking app could have taken less time because the second team was not actually searching for the item, they were just following a route found by the previous team member.

In the first two outcomes the hypothesis would be rejected, because the tracking app made no difference or was actually detrimental to the outcome. In the third outcome the hypotheses is confirmed.

Next Steps:

Based on the outcomes of the experiment new questions can be asked, and new experiments formulated.

e.g.

- What happens to times when team members can see their team mates routes, but don’t have to follow them?
- What happens to times when you increase items?
- What happens to times when you increase teams?

Potential uses:

Networking of resources - Finding out where resources are available and when.
Secondary processes - Some items need processing and this can be coordinated.

**Photos of Practical Experiments**
![]({{site.baseurl}}/photo_B0_grid2.jpg)

**Diagram of Biology Week Zero**

I constructed a diagram to help illustrate the week and how all the elements connect. I've gone into more detail on my reflections below.


![]({{site.baseurl}}/bo_diagram3.jpg)


**Reflections**


**Biology within the design process**
I personally haven’t used biology much in my own practice. I’d like to change this, and use it as a stronger influence. Getting inspiration from nature, using biological techniques and modelling processes that are more sustainable are all areas I really want to work with.
The whole idea of mixing disciplines is very powerful, combing ideas that at first might not appear to combine well however can lead to new perspectives. For example a mobile, wearable slime mould container made of glass also known as BioVer: Animated Adornment by Jessica Dias. It is a beautiful creation which considers humans current ability to observe and connect to the natural world, and offers alternative models.  [biocentricdesigngroup](https://www.biocentricdesigngroup.com/copy-of-biocatalytic-cell)

The whole relationship as designers that we have with the materials and their lifecycles is another area which I think is massively important. I really like the analogy of designers taking more inspiration from chefs. We need to know every aspect of a material, how it works, how it combines with other materials, and how it reacts under different circumstances. Mette Bek-Anderson discussed the issues and challenges of using sustainable materials and circular design approaches. That there are no guides, and no models to follow. We need to make our own path, keep asking questions e.g.[asknature](https://asknature.org) and keep finding out what different organisations are doing around the world to either make materials or find uses for them. I’d like to find what composite materials can be made from more natural materials. A lot of digital fabrication processes use materials bonded with toxic glues and resins. Are there alternatives that have similar performance but are not as dangerous to us or the environment?

**Open and Transparent Information**
The whole methodology behind DIY Biology and our Biology Zero week is open source information, knowledge sharing and creating easy access to these technologies. I think this is a critical aspect because if information is open, then it can be analysed more easily. If researches protect their work, or companies keep it hidden for commercial reasons then that can lead to issues when trying to predict what the consequences of using these new technologies will be. It can lead to suspicion, and potentially misuse.

**Who makes the laws**
This is a big topic that perhaps with my current knowledge I’m not best placed to tackle. However there are laws which dictate what research is possible and want isn’t. E.g the genetic modification of food is highly controlled and different counties have different laws on what is allowed and what isn’t. These issues are highly complex, and we are proving as a species that we are not great at looking after our environment and the organisms living in it. Laws are often so connected with economic drivers, rather than social or environmental ones, and that can often be a major problem. Society needs to drive decisions, rather than corporations and companies. Which I will also reflect on in future points.  

**Corporate and commercial agendas**
Corporations need to make money, this often outweighs other factors like sustainability or ethical considerations. I don’t think they should be the driving force behind research. Research needs to be independent.

**Public perception and acceptance**
New technologies are being developed so fast many people can’t always keep up with advances.  Some they are quick to accept like smart phones, some people are more suspicious of e.g. GM food. What is interesting is both have good and bad aspects, and also have untold consequences we don’t fully understand yet. Factors such as education, the media, convenience and costs all play a role in how certain technologies are accepted. Nuria told us about a GM modified grass that turns red when planted over land mines, meaning that the mines can be found easily and removed. The grass has been genetically modified and that seems to be for a very positive application.

**Nature of Technology**
Technology is not in itself good or bad, however it does depend a huge amount on context. All technologies can be used to help makes lives better or worse, and even that statement is subjective. This leads to some big questions surrounding how we use new developments.

**Just because we can do something does that mean we should?**
Nuria described the possibility of allowing humans to photosynthesise, which in theory could be possible. Whether we could get enough energy from sunlight is unlikely, if we could however, I challenge whether it would still be beneficial. What would it do to us if we no longer had to eat and could just photosynthesise to get energy? Yes we need to eat, but as social creatures our societies have developed around that common trait in all of us. If it suddenly became unnecessary what would the do to our mental health, or friendships, or physical stability.

**Discovery without consequence?**
There are no discoveries that have been made, which don’t in some ways have unforetold consequences. Some have been great, some not so much. I think that is one of the most challenging things, that often we just don’t know. In trying to tackle one problem could we be inadvertently be causing another. This doesn’t mean that we should avoid making new discoveries, but the considerations of their affects are always going to be difficult.

**DIY Biology**
All these topics come back to what we have been doing this week. That people can start considering all these issues, and doing things themselves. I didn’t really appreciate issues of sterility before. I know now however that with a camping stove I can keep an area around my workspace sterile, and using a pressure cooker I have an autoclave.

The most important part of it for me is that people are given enough information to be able ask the right questions, find out where information is, and know who to ask if they need help. I’m not going to be an expert biologist. I now know though that I can do some experiments myself, and some I will need to get the professionals to do for me. There is also so many people around the world doing amazing research, so it isn’t always about me repeating it, it’s accessing the information to support the work I’m interested in.

**Useful Links**

- [Thelabrat](Thelabrat.com ) Biology recipes
- [Hacteria](https://www.hackteria.org) Open source biology
- [Biohack academy](http://biohackacademy.github.io) Waag societ biolab
- [Ginkobioworks](https://www.ginkgobioworks.com/) Designs custom microbes
- [Bioreactor](http://www.bkbioreactor.com/visualization/) Mapping micro biomes
